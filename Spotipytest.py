import sys
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials



class Spotipytest:

    def __init__(self):
        super().__init__()
        #get creds from env
        client_credentials_manager = SpotifyClientCredentials()
        #instanciate spotify connection
        self.sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

