### What is this repository for? ###

* framework that will use behave scenarios to interact with spotify
* Version 0.1


### How do I get set up? ###

* install libs
    pip install -r requirements.txt on dest env
* Configuration
    sample launch.json:
    `
    {
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Module",
            "type": "python",
            "request": "launch",
            "module": "behave",
                "env": {
                    "AUTHORIZATION": "Bearer BQDYmSKqCRZxNaPw7R-YqAayAdXJC0oGQwOQLkwmgtSgikCvW_ihkqtX_-oj5EAa8pHGFP-8WG4eVlH3PywNpsjGuoLOQVu6hB3Kk0047A36Ywu_ksCo6bKM2B45jO6r4britUS1Y4pPn62hdAjzf2plwVdPQTnkX15z9wEwEIgf5H5m_VWyVwDZ0ZtJ41Gz0zkq5YJf3A1xtdumVUauAZiYUpX13TZ5nvlzIWrI54g1l3CmSDuzcvEGZztPCQ-9C99ZOewBXbKFQjhuLw"
                }
            }
    ]
}
    `


* Dependencies
    see requitrements.txt



