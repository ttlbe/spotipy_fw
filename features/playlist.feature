Feature: testing spotify playlists

Scenario Outline: add tracks from file to playlist
Given api using playlist with id "<plid>"
Then gui I can verify playlist has name "<plname>"
Given api the used playlist is empty
Then gui I can verify playlist is empty
When api I add tracks from album with id "<albumid>" to used playlist
Then gui I can verify playlist according to used tracklist
Given api the used playlist is empty
Then gui I can verify playlist is empty
When api I add tracks to used playlist from file "<file>"
Then gui I can verify playlist according to used tracklist

Examples: 
| plid                      | file                | plname          | albumid                   |
| 7AWDewccxamwgTIBDrAveM    | sampleTracklist2    | testerplaylist  | 3z6aGZfwBF0obHrf3c5swg    |