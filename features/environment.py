from selenium import webdriver
from selenium.webdriver.common.keys import Keys



def before_all(context):
	opts = webdriver.ChromeOptions()
	opts.add_argument('no-sandbox')
	context.browser = webdriver.Chrome(chrome_options=opts)
	context.endpoint = "https://api.spotify.com"


def after_all(context):
	
	context.browser.quit()
	

	
