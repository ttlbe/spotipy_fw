import requests
import os
import json

@then('api shows playlists')
def api_show_playlists(context):
    resource = "/v1/me/playlists"
    resp = json.loads(requests.get(context.endpoint + resource, headers = {
       'Accept':'application/json',
       'Content-Type':'application/json',
       'Authorization':os.getenv('AUTHORIZATION')
       }
       ).text)
    # resp is now a dict
    # put breakpoint to show content
    playlists = resp.get('items')
    #TODO: get all playlists: resp.get('next')

@then('api content for playlist with id "{plid}" is shown')
def get_content_for_playlist_with_id(context, plid):
    resource = "/v1/playlists/" + plid
    resp = json.loads(requests.get(context.endpoint + resource,headers = {
       'Accept':'application/json',
       'Content-Type':'application/json',
       'Authorization':os.getenv('AUTHORIZATION')
       }
       ).text)
    # put breakpoint to see content of playlist
    content = resp.get('items')

@given('api using playlist with id "{plid}"')
def setPlaylistId(context, plid):
    #store playlist id in session memory
    context.plid = plid

@given('api empty playlist with id "{plid}"')
def emptyPlaylistId(context, plid):
    resource = "/v1/playlists/" + plid + "/tracks"
    resp = json.loads(requests.get(context.endpoint + resource,headers = {
       'Accept':'application/json',
       'Content-Type':'application/json',
       'Authorization':os.getenv('AUTHORIZATION')
       }
       ).text)
    tracks = resp.get('items')
    position = 0
    tracksToDelete = list()
    for track in tracks:
        trackToDelete = dict()
        track.get('track')
        trackToDelete["uri"] = "spotify:track:" + track.get('track').get('id')
        positions = [position]
        trackToDelete["positions"] = positions
        tracksToDelete.append(trackToDelete)
        position += 1
    reqbody = dict()
    reqbody["tracks"] = (tracksToDelete)
    reqjson = json.dumps(reqbody)
    resp = requests.delete(context.endpoint + resource, data=reqjson, headers = {
       'Accept':'application/json',
       'Content-Type':'application/json',
       'Authorization':os.getenv('AUTHORIZATION')
       }
       )
    # put breakpoint to delete 
    print("deleted")

@when('api I add tracks from album with id "{albumid}" to playlist with id "{plid}"')
def addTracksFromAlbumIdToPlaylistId(context, albumid, plid):
    #get tracks
    tracks = list()
    resource = "/v1/albums/" + albumid + "/tracks"
    payload = {"market": "BE"}
    resp = json.loads(requests.get(context.endpoint + resource, params=payload, headers = {
       'Accept':'application/json',
       'Content-Type':'application/json',
       'Authorization':os.getenv('AUTHORIZATION')
       }
       ).text)
    for track in resp.get('items'):
        resource = "/v1/playlists/" + plid + "/tracks?"
        payload = {"uris": "spotify:track:" + track.get('id')}
        resp = requests.post(context.endpoint + resource, params=payload, headers = {
       'Accept':'application/json',
       'Content-Type':'application/json',
       'Authorization':os.getenv('AUTHORIZATION')
            }
            )
        #put breakkpoint to show adding in GUI
        tracks.append(track.get('id'))
        print(resp)
    context.tracks = tracks
    

@when('api I add tracks from album with id "{albumid}" to used playlist')
def addTracksFromAlbumIdToPlaylist(context, albumid):
    addTracksFromAlbumIdToPlaylistId(context, albumid, context.plid)


@when('api I print trackids for playlist with id "{plid}" to file "{fname}"')
def printTrackIdsForPlaylistId(context, plid, fname):
    resource = "/v1/playlists/" + plid
    resp = json.loads(requests.get(context.endpoint + resource,headers = {
       'Accept':'application/json',
       'Content-Type':'application/json',
       'Authorization':os.getenv('AUTHORIZATION')
       }
       ).text)
    tracks = resp.get('tracks').get('items')
    trackids = list()
    for track in tracks:
        #put breakpoint to see adding of trackid's
        trackids.append(track.get('track').get('id'))
    with open("data/tracklist/" + fname + ".json", "w") as f:
        f.write(json.dumps(trackids))
    


@when('api I print trackids for used playlist to file "{fname}"')
def printTrackIdsForPlaylist(context, fname):
    printTrackIdsForPlaylistId(context, context.plid, fname)

@when('api I add tracks to playlist with id "{plid}" from file "{fname}"')
def addTracksFromFileToPlaylistId(context, plid, fname):
    tracks = list()
    with open("data/tracklist/" + fname + ".json", "r") as f:
        tracks = json.loads(f.readlines()[0])
    for track in tracks:
        resource = "/v1/playlists/" + plid + "/tracks?"
        payload = {"uris": "spotify:track:" + track}
        resp = requests.post(context.endpoint + resource, params=payload, headers = {
       'Accept':'application/json',
       'Content-Type':'application/json',
       'Authorization':os.getenv('AUTHORIZATION')
            }
            )
        #put breakkpoint to show adding in GUI
        print(resp)
    context.tracks = tracks




@when('api I add tracks to used playlist from file "{fname}"')
def addTracksFromFileToPlaylist(context, fname):
    addTracksFromFileToPlaylistId(context, context.plid, fname)


@given('api the used playlist is empty')
def emptyPlaylist(context):
    emptyPlaylistId(context, context.plid)
    