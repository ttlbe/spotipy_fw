import selenium
from selenium.webdriver.common.by import By
import requests
import json
import os
from selenium.common.exceptions import *

@then('gui I can verify playlist according to used tracklist')
def gui_verify_playlist(context):
    url = "https://open.spotify.com/playlist/" + context.plid
    context.browser.get(url)
    foundTracks = list()
    # Gather all titles from GUI
    for track in context.browser.find_elements(By.CSS_SELECTOR, 'div.tracklist-name'):
        foundTracks.append(track.text.lower())
    if len(foundTracks) != len(context.tracks):
        raise AssertionError("len(foundTracks)(" + str(len(foundTracks)) + ") != len(context.tracks)(" + str(len(context.tracks)) + ")" )
    for track in context.tracks:
        resource = "/v1/tracks/" + track
        resp = json.loads(requests.get(context.endpoint + resource,headers = {
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization':os.getenv('AUTHORIZATION')
            }
        ).text)
        if not (resp.get('name').lower() in foundTracks):
            raise AssertionError(resp.get('name') + " not in list of tracks found in gui")

@then('gui I can verify playlist has name "{name}"')
def gui_verify_playlist_has_name(context, name):
    url = "https://open.spotify.com/playlist/" + context.plid
    context.browser.get(url)
    playlistTitle = context.browser.find_element(By.CSS_SELECTOR, "h1[as='h1']").text
    if not name == playlistTitle:
        raise AssertionError("name(" + name + ") != playlistTitle(" + playlistTitle) + ")"

@then('gui I can verify playlist is empty')
def gui_verify_playlist_is_empty(context):
    url = "https://open.spotify.com/playlist/" + context.plid
    context.browser.get(url)
    try:
        context.browser.find_element(By.CSS_SELECTOR, "div.contentSpacing h1")
        pass
    except NoSuchElementException as ex:
        raise AssertionError("playlist does not appear to be empty")
     
